<?php

/**
 * @file
 * rules_tweaks_emptylist.module
 */

module_load_include('inc', 'rules', 'modules/data.rules.inc');

/**
 * Implements hook_rules_action_info().
 */
function rules_tweaks_emptylist_rules_action_info() {
  $return['rules_tweaks_emptylist'] = array(
    'label' => t('Create empty list'),
    'named parameter' => TRUE,
    'parameter' => array(
      'type' => array(
        'type' => 'text',
        'label' => t('Type'),
        'options list' => 'rules_tweaks_emptylist_types',
        'description' => t('Specifies the type of the variable that should be added.'),
        'restriction' => 'input',
      ),
    ),
    'provides' => array(
      'variable_added' => array(
        'type' => 'unknown',
        'label' => t('Added variable'),
      ),
    ),
    'group' => t('Rules Tweaks'),
    'callbacks' => array(
      'form_alter' => 'rules_tweaks_emptylist_type_form_alter',
      'validate' => 'rules_tweaks_emptylist_type_validate',
    ),
  );
  $return['rules_tweaks_emptylist_fix'] = array(
    'label' => t('Fix empty list to not be null (buggy!)'),
    'parameter' => array(
      'list' => array(
        'type' => 'list',
        'label' => t('List'),
        'description' => t('The list to fix.'),
        'wrapped' => TRUE,
      ),
    ),
    'group' => t('Rules Tweaks'),
  );
  $return['rules_tweaks_emptylist_set'] = array(
    'label' => t('Set any list to empty (buggy!)'),
    'parameter' => array(
      'list' => array(
        'type' => 'list',
        'label' => t('List'),
        'description' => t('The list to set to empty.'),
        'wrapped' => TRUE,
      ),
    ),
    'group' => t('Rules Tweaks'),
  );
  return $return;
}

/**
 * Action callback: Create.
 */
function rules_tweaks_emptylist() {
  return array('variable_added' => array());
}

/**
 * Options callback for list types.
 *
 * @return array
 */
function rules_tweaks_emptylist_types() {
  module_load_include('inc', 'rules', 'modules/data.rules');
  $all_types = rules_data_action_variable_add_options();
  $list_label = t('List', array(), array('context' => 'data_types'));
  $list_types = $all_types[$list_label];
  return $list_types;
}

/**
 * Info alteration callback. @see rules_action_variable_add_info_alter().
 *
 * Care that the provided variable has the configured type.
 */
function rules_tweaks_emptylist_info_alter(&$element_info, RulesAbstractPlugin $element) {
  if (isset($element->settings['type']) && $type = $element->settings['type']) {
    $element_info['provides']['variable_added']['type'] = $type;
  }
}

/**
 * Action callback: Fix empty list.
 */
function rules_tweaks_emptylist_fix(EntityListWrapper $list) {
  if (is_null($list->value())) {
    rules_tweaks_emptylist_set($list);
  }
}

/**
 * Action callback: Set list empty.
 */
function rules_tweaks_emptylist_set(EntityListWrapper $list) {
  foreach (range(0, $list->count()-1) as $i) {
    $list->offsetUnset($i);
    dpm($list->value(), $i);
  }
}

/**
 * Custom validate callback for actions that rely on a configured type.
 * 
 * Copied from @see rules_action_create_type_validate().
 */
function rules_tweaks_emptylist_type_validate($element) {
  if (!isset($element->settings['type'])) {
    throw new RulesIntegrityException(t('Invalid type specified.'), array($element, 'parameter', 'type'));
  }
}

/**
 * Form alter callback for actions relying on the entity type or the data type.
 *
 * Copied from @see rules_action_type_form_alter().
 */
function rules_tweaks_emptylist_type_form_alter(&$form, &$form_state, $options, RulesAbstractPlugin $element) {
  $first_step = empty($element->settings['type']);
  $form['reload'] = array(
    '#weight' => 5,
    '#type' => 'submit',
    '#name' => 'reload',
    '#value' => $first_step ? t('Continue') : t('Reload form'),
    '#limit_validation_errors' => array(array('parameter', 'type')),
    '#submit' => array('rules_action_type_form_submit_rebuild'),
    '#ajax' => rules_ui_form_default_ajax(),
  );
  // Use ajax and trigger as the reload button.
  $form['parameter']['type']['settings']['type']['#ajax'] = $form['reload']['#ajax'] + array(
      'event' => 'change',
      'trigger_as' => array('name' => 'reload'),
    );

  if ($first_step) {
    // In the first step show only the type select.
    foreach (element_children($form['parameter']) as $key) {
      if ($key != 'type') {
        unset($form['parameter'][$key]);
      }
    }
    unset($form['submit']);
    unset($form['provides']);
    // Disable #ajax for the first step as it has troubles with lazy-loaded JS.
    // @todo: Re-enable once JS lazy-loading is fixed in core.
    unset($form['parameter']['type']['settings']['type']['#ajax']);
    unset($form['reload']['#ajax']);
  }
  else {
    // Hide the reload button in case js is enabled and it's not the first step.
    $form['reload']['#attributes'] = array('class' => array('rules-hide-js'));
  }
}
