<?php

/**
 * @file
 * rules_tweaks_list.module
 */

/**
 * Implements hook_rules_action_info().
 */
function rules_tweaks_list_rules_action_info() {
  $return['rules_tweaks_list_add'] = array(
    'label' => t('Get the sum of two lists'),
    'group' => t('Rules Tweaks'),
    'parameter' => array(
      'list1' => array(
        'type' => 'list',
        'label' => t('List 1', array(), array('context' => 'data_types')),
        'description' => t('The list to add to.'),
        'allow null' => TRUE,
      ),
      'list2' => array(
        'type' => 'list',
        'label' => t('List 2', array(), array('context' => 'data_types')),
        'description' => t('The list to add.'),
        'allow null' => TRUE,
      ),
    ),
    'provides' => array(
      'result_list' => array(
        'type' => 'list',
        'label' => t('Result list', array(), array('context' => 'data_types')),
        'description' => t('The sum list'),
      ),
    ),
    'callbacks' => array(
      'info_alter' => 'rules_tweaks_list_2to1_info_alter',
    ),
  );
  $return['rules_tweaks_list_diff'] = array(
    'label' => t('Get the difference of two lists'),
    'group' => t('Rules Tweaks'),
    'parameter' => array(
      'list1' => array(
        'type' => 'list',
        'label' => t('List 1', array(), array('context' => 'data_types')),
        'description' => t('The list to remove from.'),
        'allow null' => TRUE,
      ),
      'list2' => array(
        'type' => 'list',
        'label' => t('List 2', array(), array('context' => 'data_types')),
        'description' => t('The list to remove.'),
        'allow null' => TRUE,
      ),
    ),
    'provides' => array(
      'result_list' => array(
        'type' => 'list',
        'label' => t('Result list', array(), array('context' => 'data_types')),
        'description' => t('The difference list'),
      ),
    ),
    'callbacks' => array(
      'info_alter' => 'rules_tweaks_list_2to1_info_alter',
    ),
  );
  return $return;
}

/**
 * Action callback: List add.
 */
function rules_tweaks_list_add($list1, $list2) {
  // Be generous.
  if (!isset($list1)) {
    $list1 = array();
  }
  if (!isset($list2)) {
    $list2 = array();
  }
  $result = array_merge($list1, $list2);
  return array('result_list' => $result);
}

/**
 * Action callback: List diff.
 */
function rules_tweaks_list_diff($list1, $list2) {
  // Be generous.
  if (!isset($list1)) {
    $list1 = array();
  }
  if (!isset($list2)) {
    $list2 = array();
  }
  $result = array_diff($list1, $list2);
  $result = array_values($result);
  return array('result_list' => $result);
}

/**
 * Info alter callback for actions that eat 2 lists and emit a same type list.
 *
 * @param array $element_info
 * @param \RulesAbstractPlugin $element
 */
function rules_tweaks_list_2to1_info_alter(&$element_info, RulesAbstractPlugin $element) {
  // Update the required type for the list item if it is known.
  $element->settings += array('list1:select' => NULL, 'list2:select' => NULL);
  if ($wrapper1 = $element->applyDataSelector($element->settings['list1:select'])) {
    $element_info['parameter']['list2']['type'] = $wrapper1->type();
    $element_info['provides']['result_list']['type'] = $wrapper1->type();
  }
}
