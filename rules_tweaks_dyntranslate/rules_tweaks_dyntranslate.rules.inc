<?php

/**
 * @file
 * rules_tweaks_dyntranslate.module
 */

/**
 * Implements hook_rules_action_info().
 */
function rules_tweaks_dyntranslate_rules_action_info() {
  $actions = array(
    'rules_tweaks_dyntranslate' => array(
      'label' => t('Translate dynamic text (bad behavior!)'),
      'group' => t('Rules Tweaks'),
      'parameter' => array(
        'text' => array(
          'type' => 'text',
          'label' => t('Text'),
          'description' => t('The text to translate.'),
          'translatable' => TRUE,
        ),
        'language' => array(
          'type' => 'token',
          'label' => t('Language'),
          'description' => t('The language to translate the text into.'),
          'options list' => 'entity_metadata_language_list',
          'default mode' => 'select',
        ),
      ),
      'provides' => array(
        'text' => array('type' => 'text', 'label' => t('The translated text')),
      ),
    ),
  );
  return $actions;
}

/**
 * Action callback: Translate.
 */
function rules_tweaks_dyntranslate($text, $language) {
  $options = array(
    'context' => 'rules_tweaks_dyntranslate',
    'language' => $language,
  );
  return array('text' => t($text, array(), $options));
}

/**
 * Implements the form_alter callback for the "Translate a text" action to set a default selector.
 */
function rules_tweaks_dyntranslate_form_alter(&$form, &$form_state, $options, $element) {
  if (isset($form['parameter']['language']['settings']['language:select']) && empty($element->settings['language:select'])) {
    $form['parameter']['language']['settings']['language:select']['#default_value'] = 'site:current-page:language';
  }
}

